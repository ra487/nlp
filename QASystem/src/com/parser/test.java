package com.parser;

import java.util.List;

import com.arorarahul.lucene.Sentence;

public class test {
	public static void main(String[] args) {		
		Sentence s = new Sentence("The victims were workers at the factory, which produced skyrockets. Vesuvius last erupted in 1944.");
		List<String> str = s.getSentenceTerms();
		List<String> posStr = s.getSentenceTermsPOS();
		List<String> nerStr = s.getSentenceTermsNER();
		for (String S : str){
			System.out.println(S);
		}
		for (String posS : posStr){
			System.out.println(posS);
		}
		
		for (String nerS : nerStr){
			System.out.println(nerS);
		}
	}
}
