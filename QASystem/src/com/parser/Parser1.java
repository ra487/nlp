 package com.parser;
 
 import java.io.BufferedReader;
 import java.io.BufferedWriter;
 import java.io.FileReader;
 import java.io.FileWriter;
 import java.io.IOException;
 import java.util.ArrayList;
 import java.util.HashSet;
 import java.util.Iterator;
 import java.util.List;
 import java.util.Map;
 import java.util.StringTokenizer;
 
 import com.arorarahul.lucene.SearchEngine;
 import com.arorarahul.lucene.SearchResult;
 import com.arorarahul.lucene.Searcher;
 import com.parser.SParser;
 import com.wordnet.ConceptType;
 
 public class Parser1 {
 
 	public static final String QUESTIONS_FILE = "./data/test/questions.txt";
 
 	public static SParser m_sparser = new SParser();
 	public List<Question> m_questions = new ArrayList<Question>();
 	BufferedWriter subFile;
 
 	public void Init() {
 		m_sparser.Init();
 		try {
 			subFile = new BufferedWriter(new FileWriter("submissionParser_Test" +
 					".txt"));
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
 	}
 
 	// parse doc to get documents
 	public void GetQuestions() throws IOException {
 		BufferedReader fp = new BufferedReader(new FileReader(QUESTIONS_FILE));
 
 		boolean bquest = false;
 		int num = 0;
 
 		while(true)
 		{
 			String line = fp.readLine();
 			if(line == null) break;
 
 			StringTokenizer st = new StringTokenizer(line,"\n\r");
 			if(st.countTokens() <= 0) continue;
 			String txt = st.nextToken();
 			if(txt.isEmpty() == false) {
 				if(txt.contains("<num>")) {
 					String[] p = txt.split(":");
 					num = atoi(p[1].trim());
 				} else if(txt.contains("<desc>")) {
 					bquest = true;
 				} else if(bquest == true) {
 					Question quest = new Question();
 					quest.m_question = txt;
 					quest.m_mainQuestWordIndx = 0;
 					quest.questNum = num;
 					m_questions.add(quest);
 					bquest = false;
 
 					// split question into words
 					quest.m_questWords = m_sparser.GetSentWords(txt);
 					quest.m_questPos = m_sparser.GetPosTags(txt);
 					quest.m_questNER = m_sparser.GetNERTags(txt);
 					quest.m_dependencyTree = m_sparser.GetDependencyTree(txt);
 
 					System.out.println("question " + quest.m_question + " num " + num);
 				}
 			}
 
 		}
 	}
 
 	public void Run() {
 		for(int i = 0; i < m_questions.size(); i++) {
			RunQA(m_questions.get(i));
			try{
				RunQA(m_questions.get(i));
			}
			catch (OutOfMemoryError e) {
				System.out.println("What dafuq happened? I alloted 4GB of memory.");
				int j = i + 400;
				System.out.print(""+j);
				System.out.print(" top_docs."+j);
				System.out.print(" nil\n");
			}
 		}
 	}
 
 	// analysis each question
 	public void RunQA(Question quest) {
 		System.out.println("Question Sentence: " + quest.m_question);
 		System.out.println("Question Word: " + quest.m_questWords.get(0));
 
 		String queryStr = "";
 		Stemmer stemmer=new Stemmer();
 		Searcher S = new Searcher();
 
 		HashSet<String> queryStrSet = new HashSet<String>();
 
 		String answerEntityType = ConceptType.NONE;
 
 		String questType = "";
 
 		if(quest.m_questWords.get(quest.m_mainQuestWordIndx).equalsIgnoreCase("WHAT") ||
 				quest.m_questWords.get(quest.m_mainQuestWordIndx).equalsIgnoreCase("WHAT'S")) {
 
 			// stemmer not working in what? Discuss
 			List<String> focusWordList = quest.GetSentenceFocusWords();
 
 			//System.out.print("Question Focus: " );
 			for (String focusWord : focusWordList){
 				//System.out.print(focusWord + " ");
 				if(!focusWord.equals(focusWordList.get(focusWordList.size()-1))){
 					//queryStr = queryStr + "(" + focusWord + ") AND ";
 					queryStrSet.add(focusWord);
 				}
 				else{
 					//queryStr = queryStr + "(" + focusWord + ")";
 					queryStrSet.add(focusWord);
 				}
 			}
 
 			// get all relations from first word of question (how, why etc)
 			Map<Integer, String> relWords = quest.GetRelations(0);
 			if(relWords.size() == 0) {
 				System.out.println("The first word in question doesnt have any relations - Wow :O");
 				return;
 			}
 
 			// get the first relation - as mostly it wud have only one relation
 			int indx = 0;
 			String relation = "";
 			for(Integer key: relWords.keySet())	{
 				indx = key;
 				relation = relWords.get(key);
 			}
 			String ansTypeWord = "";
 			int ansTypeWordIndx = -1;
 
 			if(quest.m_questPos.get(indx).contains("NN") || quest.m_questPos.get(indx).contains("VBN")) {
 				ansTypeWord = quest.m_questWords.get(indx);
 				ansTypeWordIndx = indx;
 				queryStrSet.add(ansTypeWord);
 			} else {
 				int relIndx = quest.GetRelation(indx, "nsubj");
 				if( relIndx >= 0 ) {
 					ansTypeWord = quest.m_questWords.get(relIndx);
 					ansTypeWordIndx = relIndx;
 					queryStrSet.add(ansTypeWord);
 				} else {
 					System.out.println("nsubj type relation not found");
 				}
 			}
 
 			// get concept
 			if (ansTypeWord.equalsIgnoreCase("population")){
 				answerEntityType = "measure";
 			}
 			else{
 				answerEntityType = ConceptType.GetClosestConcept(ansTypeWord);
 			}
 			System.out.println("Question Answer Type: " + ansTypeWord + "\nEntity: " + answerEntityType);
 		}
 		// else every other question type
 		else {
 
 			// get all relations from first word of question (how, why etc)
 			Map<Integer, String> relWords = quest.GetRelations(0);
 			if(relWords.size() == 0) {
 				System.out.println("The first word in question doesnt have any relations - Wow :O");
 				return;
 			}
 
 			// get the first relation - as mostly it wud have only one relation
 			int indx = 0;
 			String relation = "";
 			for(Integer key: relWords.keySet())	{
 				indx = key;
 				relation = relWords.get(key);
 			}
 
 			// find all NN's
 			int idxNN = quest.FindClosestWordByPOS(indx, "NN");
 
 			questType = quest.m_questWords.get(indx);
 
 			//queryStr = "(" + stemmer.magic(questType) + "*)";
 			
 			if (m_sparser.stopWords.contains(questType)){
 				
 			}else{
 				queryStrSet.add(stemmer.magic(questType) + "*");
 			}
 
 			/*for (String str : quest.GetFullEntity(idxNN)){
 				//queryStr = queryStr + " AND (" + str + ")";
 				queryStrList.add(str);
 			}*/
 			for (String str : quest.GetFullEntityR()){
 				//queryStr = queryStr + " AND (" + str + ")";
 				queryStrSet.add(str);
 			}
 
 			System.out.println("Quantity Type: " + quest.m_questWords.get(indx));
 			//System.out.println("Question Focus: " + quest.GetFullEntity(idxNN));
 			System.out.println("Question Focus: " + quest.GetFullEntityR());
 		}
 
 		String nerTag = "";
 		//Find relevant NERtag 
 		if (quest.m_questWords.get(0).equals("Who") || quest.m_questWords.get(0).equals("Whom")){
 			nerTag = "PERSON";
 		}
 		else if (quest.m_questWords.get(0).equals("Which")){
 			nerTag = "ORGANIZATION";
 		}
 		else if (quest.m_questWords.get(0).equals("When")){
 			nerTag = "DATE";
 		}
 		else if (quest.m_questWords.get(0).equals("How")){
 			nerTag = "NUMBER";
 		}
 		else if (quest.m_questWords.get(0).equals("Name")){
 			nerTag = "PERSON_LOCATION_ORGANIZATION";
 		}
 		else if (quest.m_questWords.get(0).equals("Where") || quest.m_questWords.get(0).equals("Whence") 
 				|| quest.m_questWords.get(0).equals("Whither")){
 			nerTag = "LOCATION";
 		}
 		else if (quest.m_questWords.get(0).equals("What") || quest.m_questWords.get(0).equals("Name")) {
 			if (answerEntityType.equals("entity")){
 				nerTag = "PERSON_LOCATION_ORGANIZATION";
 			}
 			else if (answerEntityType.equals("measure")){
 				nerTag = "NUMBER";
 			}
 		}
 		else {
			nerTag = "??";
			nerTag = "PERSON_LOCATION_ORGANIZATION_NUMBER";
 		}
 
 		HashSet <String> newQueryStrSet = new HashSet <String>();
 		newQueryStrSet.addAll(queryStrSet);
 
 		Iterator<String> it = newQueryStrSet.iterator();
 		while(it.hasNext()){
 			String queryStr1 = it.next();
 			if (m_sparser.stopWords.contains(queryStr1)){
 				queryStrSet.remove(queryStr1);
 			}
 		}
 
 
 		Iterator<String> itr = queryStrSet.iterator();
 
 		int i = 0;
 
 		while(itr.hasNext()){
 			queryStr += "(" + itr.next() + ")";
 			if(i < queryStrSet.size() - 1)
 				queryStr += " AND ";
 			i++;
 		}
 
 		System.out.println("QueryString: "+queryStr);
 		System.out.println("NERTag: "+nerTag);
 
 		ArrayList <SearchResult> searchResults = SearchEngine.query(S,quest.questNum,queryStr);
 
 		if (searchResults.size() == 0){
 			System.out.print(quest.questNum);
 			System.out.print(" top_docs."+quest.questNum);
 			System.out.print(" nil\n");
 		}
 
 		ArrayList<String> answers = new ArrayList<String>();
 		int count = 0;
 
 		for (SearchResult srchResult : searchResults){
 			Sentence s = new Sentence(srchResult.getSentenceString());
 			//System.out.println(srchResult.getSentenceString());
 
 			List<String> str = s.getSentenceTerms();
 			List<String> nerStr = s.getSentenceTermsNER();
 
 			String answer = "";
 
 			for (i = 0; i < nerStr.size(); i++){
 				boolean flag = false;
 				if(nerTag.equals("PERSON_LOCATION_ORGANIZATION")){
 					if(nerStr.get(i).equals("PERSON")){
 						flag = true;
 					}
 					else if(nerStr.get(i).equals("LOCATION")){
 						flag = true;
 					}
 					else if(nerStr.get(i).equals("ORGANIZATION")){
 						flag = true;
 					}
 				}
				else if(nerTag.equals("PERSON_LOCATION_ORGANIZATION_NUMBER")){
					if(nerStr.get(i).equals("PERSON")){
						flag = true;
					}
					else if(nerStr.get(i).equals("LOCATION")){
						flag = true;
					}
					else if(nerStr.get(i).equals("ORGANIZATION")){
						flag = true;
					}
					else if(nerStr.get(i).equals("NUMBER")){
						flag = true;
					}
				}
 				else if(nerStr.get(i).equals(nerTag)){
 					flag = true;
 				}
 
 				if (flag){
 					if(!str.get(i).equals("")){
 						if(!queryStrSet.contains(answer)){
 							if (!answers.contains(str.get(i))){
 								answers.add(str.get(i));
 								answer = answer + str.get(i) + " ";
 							}
 						}
 					}
 				}
 			}
 			if (!answer.equals("")){
 				//if (count < 5){
 					System.out.print(quest.questNum);
 					System.out.print(" top_docs."+quest.questNum);
 					System.out.print(" " +answer+"\n");
 					count = count + 1;
 					try {
 						subFile.write(""+quest.questNum);
 						subFile.write(" top_docs."+quest.questNum);
 						subFile.write(" " +answer+"\n");
 					} catch (IOException e) {
 						// TODO Auto-generated catch block
 						e.printStackTrace();
 					}
 					
 					//System.out.println(srchResult.getSentenceString());
 				//}
 			}
 			/*else{
 				System.out.print(quest.questNum);
 				System.out.print(" top_docs."+quest.questNum);
 				System.out.print(" nil\n");
 			}*/
 		}
 
 		/*String ansStr = "";
 		count = 1;
 		for (String answer : answers){
 			if(count % 9 == 0){
 				System.out.print(quest.questNum);
 				System.out.print(" top_docs."+quest.questNum);
 				System.out.print(" " +ansStr+"\n");
 				ansStr = "";
 			}
 			else{
 				if (!answer.equals("")){
 					ansStr = ansStr + " " + answer;
 				}
 			}
 			count = count + 1;
 		}
 
 		if(!ansStr.isEmpty()){
 			System.out.print(quest.questNum);
 			System.out.print(" top_docs."+quest.questNum);
 			System.out.print(" " +ansStr+"\n");
 		}*/
 	}
 
 	/**
 	 * @param args
 	 * @throws IOException 
 	 */
 	public static void main(String[] args) throws IOException {
 		Parser parser = new Parser();
 		parser.Init();
 		parser.GetQuestions();
 
 		parser.Run();
 		parser.subFile.close();
 	}
 
 	private static int atoi(String s)
 	{
 		return Integer.parseInt(s);
 	}
 
 }
